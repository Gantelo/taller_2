#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 22 14:59:33 2018

@author: giuliano
"""
#Tomo dos listas como ejemplo
lista1=[2,3,4,4,5]
lista2=[3,4,5,4,7]

def cantAparicionesSub(a,b):
#Tomo dos contadores. Uno que va a mirar a la lista 1 y otro a la lista2
    n1=0
    n2=0
    apariciones=0
#Si el elemento 0 de la lista1 es igual al elemento 0 de la lista2, sumo 1 a apariciones    
    while n1<=len(a)-1:
        n2=0
        if a[n1] == b[n2]:
            apariciones += 1
#Sino, me muevo un lugar en la lista 2, y evalúo de nuevo                    
        else:
            while n2<len(b)-1 and a[n1] != b[n2]:
                n2 += 1
                if a[n1] == b[n2]:
                    apariciones += 1
#Una vez que recorrí toda la lista2 para una posicion de la lista1, me muevo un lugar en esta última y repite.                
        n1+=1
    return (apariciones)

print (cantAparicionesSub(lista1,lista2))