#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 12:54:10 2018

@author: giuliano
"""
#Genero algunas listas para usar como ejemplos
lista= [1,2,3,4,5,4,3,2,1]
lista2= [1,2,3,3,4,5,4,3,2,1]
lista3= [2,3,4,2,5,6,7,1]
prueba=[1,2,3,2]
def listaTriangular(a):
    n=0
#Pido que se recorra la lista siempre que ésta vaya creciendo. Si no es creciente de entrada, False
    if a[n] < a[n+1]:
        while n<len(a)-1 and a[n]< a[n+1]:
            n+=1            
#Cuando sale del while, pido que siga recorriendo si es decreciente de ahí en más. Sino, False
        if a[n] > a[n+1]:
            while n<len(a)-1 and a[n] > a[n+1]: 
                n+=1                
            return True
        else:
            return False
    else:
        return False
    
print (listaTriangular(lista2))